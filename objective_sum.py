# Python program to check for the target_sum condition to be satisified
# This code is contributed by __Devesh Agrawal__
# Modified by rlucerga to correct naming conventions, add type hinting and
# use np Quicksort

import numpy as np


def objective_sum(a: np.array, target_sum: int) -> bool:
    """
    Finds if an array has two numbers that sum a given target value
    :param a:           the array to check
    :param target_sum:  the target value for the sum
    :return:            a bool with the result of the search
    """
    # sort the array
    a = np.sort(a, kind='quicksort')
    left = 0
    right = len(a) - 1

    # traverse the array for the two elements
    while left < right:
        if a[left] + a[right] == target_sum:
            return True
        elif a[left] + a[right] < target_sum:
            left += 1
        else:
            right -= 1
    return False


def check_objective_sum(a: np.array, n: int) -> None:
    # Driver program to test the functions
    if objective_sum(a, n):
        print("Array has two elements with the given target_sum")
    else:
        print("Array doesn't have two elements with "
              "the given target_sum")


if __name__ == '__main__':
    input_array = np.array([1, 4, 45, 6, 10, -8])
    target_n = 16
    check_objective_sum(input_array, target_n)
